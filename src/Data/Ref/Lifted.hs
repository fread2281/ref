{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses, FlexibleContexts, NoMonomorphismRestriction #-}
-----------------------------------------------------------------------------
-- |
-- Lifted versions of functions from Data.Ref
----------------------------------------------------------------------------

module Data.Ref.Lifted 
    (
        newRef, newRef',
        readRef, readRef',
        writeRef, writeRef',
        modifyRef, modifyRef',
        R.Ref, R.RefM) where

import Control.Monad.Base
import qualified Data.Ref as R

newRef = liftBase . R.newRef
newRef' = liftBase . R.newRef'
readRef = liftBase . R.readRef
readRef' = liftBase . R.readRef'
writeRef a b = liftBase $ R.writeRef a b
writeRef' a b = liftBase $ R.writeRef' a b
modifyRef a b = liftBase $ R.modifyRef a b
modifyRef' a b = liftBase $ R.modifyRef' a b

